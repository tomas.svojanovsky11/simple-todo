function getRemoveIcon () {
    const span = document.createElement('span')
    span.appendChild(document.createTextNode('delete'))
    span.setAttribute('class', 'material-icons remove-button')

    return span
}

function setTotalNumber (totalNumber, length) {
    totalNumber.innerHTML = `Number of todos: ${length}`
}

function setCompletedNUmber (totalNumber, length) {
    totalNumber.innerHTML = `Completed: ${length}`
}

function getCheckbox () {
    const input = document.createElement('input')
    input.setAttribute('type', 'checkbox')
    input.setAttribute('class', 'complete-button')

    return input
}

function getText (value) {
    const div = document.createElement('div')
    const text = document.createTextNode(value)
    div.appendChild(text)

    return div
}

document.addEventListener('DOMContentLoaded', () => {
    const todoList = document.querySelector('.todo-list')
    const totalNumber = document.querySelector('.total-number')
    const completeNumber = document.querySelector('.completed')
    const addButton = document.querySelector('.add-button')
    const addIcon = document.querySelector('.add-icon')
    const inputGroup = document.querySelector('.input-group')
    const input = inputGroup.querySelector('input')
    let showInputGroup = false

    addButton.addEventListener('click', () => {
        input.value = ''
        inputGroup.style.visibility = showInputGroup ? 'hidden' : 'visible'
        input.focus()
        showInputGroup = !showInputGroup
    })

    addIcon.addEventListener('click', () => {
        if (input.value) {
            const removeIcon = getRemoveIcon()
            const checkbox = getCheckbox()
            const text = getText(input.value)

            const li = document.createElement('li')
            li.appendChild(checkbox)
            li.appendChild(text)
            li.appendChild(removeIcon)
            todoList.appendChild(li)
            input.value = ''

            const removeButtons = document.querySelectorAll('.remove-button')
            removeIcon.addEventListener('click', (event) => {
                const { target } = event
                const { parentElement: parent } = target

                parent.remove()
                setTotalNumber(totalNumber, removeButtons.length - 1)
            })

            checkbox.addEventListener('click', (event) => {
                const { checked, parentElement: parent } = event.target
                if (checked) {
                    parent.setAttribute('class', 'complete-todo')
                } else {
                    parent.removeAttribute('class', 'complete-todo')
                }
                const completeTodos = document.querySelectorAll('.complete-todo')

                setCompletedNUmber(completeNumber, completeTodos.length)
            })

            setTotalNumber(totalNumber, removeButtons.length)
        }
    })
})
